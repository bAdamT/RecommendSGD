﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LumenWorks.Framework.IO.Csv;
using Microsoft.Win32;

namespace RecommededMatrix
{
  class CsvExtractor
  {
    private char delimiter;
    private (int UserIdKey, int MovieIdKey, int RatingKey) columnKeys;

    public CsvExtractor(char delimiter, (int UserIdKey, int MovieIdKey, int RatingKey) columnKeys)
    {
      this.delimiter = delimiter;
      this.columnKeys = columnKeys;
    }

    public IEnumerable<(int UserId, int MovieId, int Rating)> Read(string pathToCsv)
    {
      using (CsvReader csv = new CsvReader(new StreamReader(pathToCsv), false, delimiter:' '))
      {
        return csv.Select(record => (
          int.Parse(csv[columnKeys.UserIdKey]) - 1, 
          int.Parse(csv[columnKeys.MovieIdKey]) - 1, 
          int.Parse(csv[columnKeys.RatingKey])
          ))
          //Must iterate or else it will be disposed
          .ToList();
      }
    }
  }
}
