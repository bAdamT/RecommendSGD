﻿#define NORMALIZE
#define REGULATION
#define ERROR
#define EXTEND
#define VALIDATE

using System;
using System.Diagnostics;
using System.Linq;

namespace RecommededMatrix
{
  class Program
  {
    private static int maxIterations = 10;
    private static int representation = 100;
    private static double epsilon = 0.01;
    private static double regulation = 0.001;
    private static int validMod = 5;

    static void Main(string[] args)
    {
      //Helpers
      var dataSource = @"C:\Git Repos\ML\RecommededMatrix\RecommededMatrix\Resources\moviedata\ratings.train";
      var extractor = new CsvExtractor(' ', (0, 1, 2));
      var data = new Datas();

      //Read data
      var rawData = extractor.Read(dataSource).ToList();


      //Separate into train and validate
      var train = rawData.ToList();
      //train = train.Where((x, i) => x.UserId < 4000 && x.MovieId < 4000).ToList();
      int userCount = train.Select(k => k.UserId).Max() + 1;
      int movieCount = train.Select(k => k.MovieId).Max() + 1;
      train = train.Where((x, i) => i % validMod != 0).ToList();
      var check = train;
#if VALIDATE
      var validate = train.Where((x, i) => i % validMod == 0).ToList();
      check = validate;
#endif 

      foreach (var record in train)
        data.Ratings[(record.UserId, record.MovieId)] = record.Rating;

#if NORMALIZE
      //Remove bias
      data.Bias = data.Ratings.Values.Average();
      foreach (var key in data.Ratings.Keys.ToList())
        data.Ratings[key] -= data.Bias;
      //Remove user bias
      data.UserBias = data.Ratings.GroupBy(x => x.Key.UserId).ToDictionary(y => y.Key, y => y.Average(v => v.Value));
      foreach (var key in data.Ratings.Keys.ToList())
        data.Ratings[key] -= data.UserBias[key.UserId];
      //Remove movie bias
      data.MovieBias = data.Ratings.GroupBy(x => x.Key.MovieId).ToDictionary(y => y.Key, y => y.Average(v => v.Value));
      foreach (var key in data.Ratings.Keys.ToList())
        data.Ratings[key] -= data.MovieBias[key.MovieId];
#endif

      //Initialize learning
      Random rand = new Random();
#if NORMALIZE
      var bounds = (Lower: 10, Upper: 100, Range: 100.0);
#else
      var bounds = (Lower: 100, Upper: 500, Range: 100.0);
#endif


      var reference = 1.0 / Math.Pow(representation, 0.5);
      data.Users = new double[userCount][];
      for (var u = 0; u < userCount; u++)
        data.Users[u] = Enumerable.Repeat(reference, representation).Select(r => r * rand.Next(bounds.Lower, bounds.Upper) / bounds.Range).ToArray();
      data.Movies = new double[movieCount][];
      for (var m = 0; m < movieCount; m++)
        data.Movies[m] = Enumerable.Repeat(reference, representation).ToArray();

      //Learn
      Console.WriteLine("Start learning...");
      Stopwatch sw = new Stopwatch();
      sw.Start();
      for (int iter = 0; iter < maxIterations; iter++)
      {
        Console.WriteLine($"Starting iteration {iter}...");
        //Update values for existing ratings
        foreach (var entry in data.Ratings)
        {
          var user = entry.Key.UserId;
          var movie = entry.Key.MovieId;
          var truth = entry.Value;
          var userVector = data.Users[user];
          var movieVector = data.Movies[movie];
          var newUserVector = new double[representation];
          var newMovieVector = new double[representation];

          var guess = Dot(userVector, movieVector);
          var error = guess - truth;

          for (int i = 0; i < representation; i++)
          {
            //Calculate new value
            newUserVector[i] = userVector[i] - error * epsilon * movieVector[i];
            newMovieVector[i] = movieVector[i] - error * epsilon * userVector[i];
            //Regulation
#if REGULATION
            newUserVector[i] -= regulation * userVector[i];
            newMovieVector[i] -= regulation * movieVector[i];
#endif
            if (double.IsNaN(newUserVector[i]) || double.IsInfinity(newUserVector[i]) ||
                double.IsNaN(newMovieVector[i]) || double.IsInfinity(newMovieVector[i]))
              throw new ArgumentException();
          }
          // Console.WriteLine($"Guess went from {guess:f2} to {Dot(newUserVector, newMovieVector):f2} based on truth {truth:f2} for ({user},{movie})!");

          //Update
          data.Users[user] = newUserVector;
          data.Movies[movie] = newMovieVector;
        }
        Console.WriteLine($"Done iteration {iter} at {sw.Elapsed.TotalSeconds}!");
#if ERROR
        Console.WriteLine($"Goal is {Error(data, check.ToArray())}.");
#endif
#if EXTEND
        if (iter + 1 == maxIterations)
        {
          Console.WriteLine($"Current MSE is {Error(data, check.ToArray()):f4}.");
          Console.WriteLine("Want to add 10 extra iterations?");
          var key = Console.ReadKey().Key;
          if (key == ConsoleKey.Y)
            maxIterations += 10;
        }
#endif
      }
      Console.WriteLine("Finished learning!");

      Console.WriteLine("Start validation...");

      var stats = (Error: 0, All: 0);
      foreach (var (user, movie, truth) in check)
      {
        var guess = Dot(data.Users[user], data.Movies[movie]);
        var biasedGuess = ValueFor(user, movie, data);
        var error = biasedGuess - truth;

        stats.All++;
        if (Math.Abs(error) > 0.5d)
        {
          stats.Error++;
          Console.WriteLine($"{error:F2} = {biasedGuess:F2} - {truth:F2}" +
#if NORMALIZE
                      $" from Learnt Value: {guess:F2}, Bias: {data.Bias:F2}, User bias: {data.UserBias[user]:F2}, Movie bias: {data.MovieBias[movie]:F2}" +
#endif
                      $".");

        }

      }
      Console.WriteLine("Finished validation!");

      Console.WriteLine($"Results: {stats.Error}/{stats.All} = {(double)stats.Error / stats.All}.");
      Console.WriteLine($"MSE IS {Error(data, check.ToArray())}");
      while (true)
      {
        if (Console.ReadKey().Key == ConsoleKey.X)
          break;
      }

    }

    private static double Dot(double[] v1, double[] v2)
    {
      return v1.Zip(v2, (x, y) => x * y).Sum();
    }

    private static double ValueFor(int u, int m, Datas data)
    {
      return Dot(data.Users[u], data.Movies[m]) +
#if NORMALIZE
             +data.MovieBias[m] + data.UserBias[u] + data.Bias
#endif
        ;
    }

    private static double Goal(Datas data)
    {
      var mse = data.Ratings.Select(r => (r.Value - Dot(data.Users[r.Key.UserId], data.Movies[r.Key.MovieId]))).Sum(v => v * v);
#if REGULATION
      var re = regulation * (
                 data.Users.Sum(u => Dot(u, u)) +
                 data.Movies.Sum(m => Dot(m, m))
               );
#endif
      return mse
#if REGULATION
             + re
#endif
        ;

    }

    private static double Error(Datas data, (int UserId, int MovieId, int Rating)[] validation)
    {
      var mse = validation.Select(r => (r.Rating - ValueFor(r.UserId, r.MovieId, data))).Sum(v => v * v) / validation.Length ;
      return mse;
    }
  }
}
