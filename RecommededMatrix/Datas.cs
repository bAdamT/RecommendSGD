﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecommededMatrix
{
  public class Datas
  {
    public Dictionary<(int UserId, int MovieId), double> Ratings = new Dictionary<(int, int), double>();
    public double[][] Users;
    public double[][] Movies;
    public double Bias;
    public Dictionary<int, double> UserBias = new Dictionary<int, double>();
    public Dictionary<int, double> MovieBias = new Dictionary<int, double>();
  }
}
